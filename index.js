//Importar la clase express

import express from 'express';


//Crear un objeto express
const app=express();


// Crear una ruta

app.get("/bienvenida", (peticion, respuesta)=>{

    res.send("Bienvenido al mundo de backend Node.");
});

//Suma

app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)+Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

//Resta 

app.get("/restar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)-Number(peticion.query.b);

    respuesta.send(" Respuesta "+resultado.toString());
});

//Multiplicacion 

app.get("/multiplicar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)*Number(peticion.query.b);

    respuesta.send(" Respuesta "+resultado.toString());
});

//Divicion

app.get("/dividir", (peticion, respuesta)=>{

    if(Number(peticion.query.b)!=0){
    let resultado=Number(peticion.query.a)/Number(peticion.query.b);
    respuesta.send(" Respuesta "+resultado.toString());
}
if(Number(peticion.query.b)==0){
    respuesta.send("No se puede dividir entre 0");
}
});

//Modulo
app.get("/modular", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)%Number(peticion.query.b);

    respuesta.send(" Respuesta "+resultado.toString());
});


//Inicializar el servidor node

app.listen(3001);
